#!/usr/bin/env perl

# Copyright (C) 2021 samuelbarreto

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------------------------

use strict;
use warnings;
use v5.24;

# --------------------------------------------------------------------

say "seqid\tmatch\tn_match";
while (<>) {
  chomp;
  my @line = split /\t/;
  my $seq_id = $line[1];
  my @tid = (split /\|/, $seq_id);
  my $lca_map = $line[4];
  # say "$tid\t$lca_map";
  my @lca_maps = split /[[:space:]]/, $lca_map;
  foreach my $kv (@lca_maps) {
    my ($key,$val) = split /:/, $kv;
    my $cond = ($tid[2] eq $key) || ($key eq 0);
    unless ($cond) {
      say "$tid[0]\t$key\t$val";
    }
  }
}

__END__
