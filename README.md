
# krakmeeten

<!-- badges: start -->
<!-- badges: end -->

The goal of krakmeeten is to detect and spot cross-contaminated
sequences in assembled contigs or scaffold at the whole sequencing
project scale.

## Installation

You can install the released version of krakmeeten from [CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("krakmeeten")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(krakmeeten)
## basic example code
krakmeeten("etc/sample_list.tsv", "/data/krakmeeten_db/", parallel = 4)
```

## Workflow

The full workflow takes as input a file name (or a data.frame [#5]).
This file is a delimit is a delimiter-separated value file
(delim can be anything as long as it is one character long.)
containing:

1. sample names;
2. path to scaffolded or contig assembly;
3. (optionally) a taxonomic ID. If no TaxID is provided, krakmeeten
      will assign one based on a rough mash-based distance estimation
      [#4].

Krakmeeten then

0. **prepares** all input files for building a kraken2 database.
1. **builds** a kraken2 database for assignments of k-mers to nodes in the
   sample tree.
2. **query** all assembly against this database
3. **parse the results**

Parsing is done by fetching minimizer matches from the kraken2 output.
Contaminated sequences match to a node in the taxonomical tree that is
higher than expected under a vertical descent hypothesis.
